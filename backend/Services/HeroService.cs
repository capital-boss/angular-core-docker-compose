using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using webapi.Model;

namespace webapi.Services
{
    public interface IHeroesService
    {
        Task<IEnumerable<Hero>> GetHeroes();
        Task<Hero> GetHero(int id);

    }
    public class HeroesService : IHeroesService
    {
        ApplicationContext _context;

        public HeroesService(ApplicationContext context){
            _context = context;
        }
        public async Task<IEnumerable<Hero>> GetHeroes()
        {
            return await Task<IEnumerable<Hero>>.Run(
                () => {
                    Thread.Sleep(300);
                    return _context.Heroes.ToList();
                }
            );            
        }
        
        public async Task<Hero> GetHero(int id)
        {
            return await Task<Hero>.Run(
                () => {
                    Thread.Sleep(300);
                    return _context.Heroes.Where(x=> x.id == id).FirstOrDefault();
                }
            );            
        }
    }
}
