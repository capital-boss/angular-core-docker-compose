using Microsoft.EntityFrameworkCore;

namespace webapi.Model
{
    public class ApplicationContext : DbContext
    {
        //public DbSet<User> Users { get; set; }
        public DbSet<Hero> Heroes { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //optionsBuilder.UseNpgsql("Host=localhost;Port=6001;Database=heroesdb;Username=postgres;Password=password");
            optionsBuilder.UseNpgsql("Host=db;Port=5432;Database=heroesdb;Username=postgres;Password=password");            
        }

    }

    public class Hero
    {
        public int id { get; set; }
        public string name { get; set; }
    }

}